import json
from os import getenv

from fastapi import Depends, FastAPI, HTTPException
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from starlette.status import HTTP_401_UNAUTHORIZED
import pika
from pydantic import BaseModel
import uvicorn

QUEUE_NAME = 'reg_form'

class Item(BaseModel):
    reg_num: int

app = FastAPI()
security = HTTPBasic()

USERNAME = getenv("USERNAME")
PASSWORD = getenv("PASSWORD")
if not all((USERNAME, PASSWORD)):
    with open('credentials.json', 'r') as fh:
        creds = json.load(fh)
        USERNAME = creds['username']
        PASSWORD = creds['password']

CONNECTION = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
CHANNEL = CONNECTION.channel()
CHANNEL.queue_declare(queue=QUEUE_NAME)

@app.on_event("shutdown")
def shutdown_event():
    CONNECTION.close()

@app.post('/reg_form_data')
def reg_form_data(item: Item, credentials: HTTPBasicCredentials = Depends(security)):
    if credentials.username != USERNAME or credentials.password != PASSWORD:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    CHANNEL.basic_publish(exchange='', routing_key=QUEUE_NAME, body=f'{item.reg_num}')
    print(f"published {item.reg_num} to {CHANNEL}")
    return f'Reg num: {item.reg_num}'

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=9003)
    
