FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

RUN mkdir -p /app
COPY ./app/requirements.txt /app/
RUN pip install -r /app/requirements.txt
COPY ./app/main.py /app/

